package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/dlclark/regexp2"
)

// ref : https://gist.github.com/tdegrunt/045f6b3377f3f7ffa408
func visit(path string, fi os.FileInfo, err error) error {

	if err != nil {
		return err
	}

	if fi.IsDir() {
		return nil
	}

	matched, err := filepath.Match("*.motion3.json", fi.Name())

	if err != nil {
		return err
	}

	if matched {
		read, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}
		fmt.Println(path)

		pattern := regexp2.MustCompile(`\{((.*)?\n){2}(.*)LipSync((.(?!\{))*?\n)*`, 0)
		newContents, _ := pattern.Replace(string(read), "", -1, -1)

		err = ioutil.WriteFile(path, []byte(newContents), 0)
		if err != nil {
			return err
		}

	}

	return nil
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		log.Fatal("need path.")
	}
	err := filepath.Walk(args[0], visit)
	if err != nil {
		log.Fatal(err.Error())
	}
}
